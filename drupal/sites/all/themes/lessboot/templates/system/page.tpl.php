<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *advert
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
?>



<head>
   <!-- <meta name="viewport" content="width=device-width, initial-scale=1">-->


</head>

<link href='https://fonts.googleapis.com/css?family=Ubuntu:400&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet'
      type='text/css'>
<div class="main-container <?php print $container_class; ?>">
    <div class="tempfill">
        <header role="banner" class="page-header-own">

            <div class="row">
                <div class="col-md-2"><a href="http://www.workland.com.ua"><img src="<?php if ($logo): print $logo; endif; ?>"></a></div>
                <div class="col-md-6"><?php if (!empty($page['headercenter'])): print render($page['headercenter']); endif; ?></div>
                <div class="col-md-4"><?php if (!empty($page['login'])): print render($page['login']); endif; ?> </div>

                <!--Кастомная шапка сайта, от Bootstrap-->
                <div class="col-md-10">
                <div class="<?php print $container_class; ?>">

                            <?php if ($logo): ?>
                                <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>"
                                   title="<?php print t('Home'); ?>">
                                    <!--  <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/> -->
                                </a>
                            <?php endif; ?>

                            <?php if (!empty($site_name)): ?>
                                <a class="name navbar-brand" href="<?php print $front_page; ?>"
                                   title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>

                            <?php endif; ?>
                            <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>

                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target=".navbar-collapse">
                                    <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            <?php endif; ?>
                        </div>

                        <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
                            <div class="navbar-collapse collapse">
                                <nav role="navigation">


                                    <?php if (!empty($page['searchbtn'])): ?>
                                        <?php print render($page['searchbtn']); ?>
                                    <?php endif; ?>


                                </nav>
                            </div>
                        <?php endif; ?>
                    </div>
                <!--Конец кастомная шапка сайта, от Bootstrap-->


            </div>

            <?php if (!empty($site_slogan)): ?>
                <p class="lead"><?php print $site_slogan; ?></p>
            <?php endif; ?>

            <!--  <?php print render($page['header']); ?>-->


        </header>

            <div class="col-md-12">

                <!-- Выпадающее меню для Superfish -->
                <?php
                if (!empty($page['altnav'])):
                    print render($page['altnav']);
                endif;
                ?>

                <?php
                if (!empty($page['forumbtn'])):
                    print render($page['forumbtn']);
                endif;
                ?>


        </div>




        <!--Посадочное место под баннер 960x90-->
        <?php
        if (!empty($page['advert'])):
            print render($page['advert']);
        endif;
        ?>


    </div>


    <!--left sidebar-->
    <?php if (!empty($page['sidebar_first'])): ?>
        <aside class="col-sm-3" role="complementary">
            <?php print render($page['sidebar_first']); ?>
        </aside>
    <?php endif; ?>

    <!--center-->
    <?php
    preg_match_all('|\d+|', $content_column_class, $matches);
    $match = $matches[0];
    $mt = intval( $match[0]);
    $new_class = str_replace($match,  (string) ($mt+1), $content_column_class);

    ?>

    <section<?php print $new_class; ?>>

        <?php if (!empty($breadcrumb)): print $breadcrumb; endif; ?>

        <a id="main-content"></a>
        <?php print render($title_prefix); ?>

        <?php if (!empty($title)): ?>
            <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>


        <?php if (!empty($tabs)): ?>
            <?php print render($tabs); ?>
        <?php endif; ?>

        <?php print render($page['content']); ?>
    </section>

    <!--right sidebar-->
    <?php if (!empty($page['sidebar_second'])): ?>
        <aside class="col-sm-2" role="complementary">
            <?php print render($page['sidebar_second']); ?>
        </aside>
    <?php endif; ?>

</div>


<script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#superfish ul.menu").superfish({
            delay:       0,
            animation:   {opacity:'show',height:'show'},
            speed:       'fast',
            autoArrows:  true,
            dropShadows: true,
            disableHI:   true
        });
    });
</script>





<?php if (!empty($page['footer'])): ?>
    <footer class="footer <?php print $container_class; ?>">
        <?php print render($page['footer']); ?>
    </footer>



<?php endif; ?>
